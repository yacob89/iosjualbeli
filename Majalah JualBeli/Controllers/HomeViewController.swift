//
//  HomeViewController.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 9/29/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import SwiftyJSON
import EVReflection
import SDWebImage

class HomeViewController: UICollectionViewController, MyCellDelegate {
    
    var photos = Photo.allPhotos()
    var resultSet = ResultMap()
    var countable = 0
    var imageCountable = 0
    var serverURL = "http://www.majalahjualbeli.com/"
    var itemHeight = [Int]();
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let patternImage = UIImage(named: "Pattern") {
            view.backgroundColor = UIColor(patternImage: patternImage)
        }
        
        //getProduk(1)
        //getAllProduks()
        //getPins()
        getAllProducts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.countable = 0
        self.imageCountable = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        /*collectionView?.backgroundColor = UIColor.clear
        collectionView?.contentInset = UIEdgeInsets(top: 23, left: 10, bottom: 10, right: 10)
        // Set the PinterestLayout delegate
        if let layout = collectionView?.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Methods
    @IBAction func cellButtonPressed(_ sender: UIButton) {
        print("PENCET LAGI")
        var indexPath:IndexPath? = nil
        
        if let cell = sender.superview?.superview as? UICollectionViewCell {
            indexPath = (self.collectionView?.indexPath(for: cell))!
            print("INDEXPATHNYA ADALAH: \(String(describing: indexPath))")
        }
        performSegue(withIdentifier: "cellButtonSegue", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // showDetail Segue
        if segue.identifier == "cellButtonSegue" {
            // Sending the image to DetailViewController
            // Before appears in the screen.
            let detailViewController = segue.destination as! ProfileViewController
            //detailViewController.image = sender as? UIImage
        }
    }
    
    //6. Implement Delegate Method
    func btnCloseTapped(cell: AnnotatedPhotoCell) {
        print("HARUS PENCET YANG INI")
        //Get the indexpath of cell where button was tapped
        let indexPath = self.collectionView?.indexPath(for: cell)
        print(indexPath!.row)
    }
    
    func getPins() {
        let url = URL(string: "http://www.majalahjualbeli.com/api/pin/get-wedding")
        let data = try? Data(contentsOf: url!)
        
        do {
            let myStructDictionary = try JSONDecoder().decode([String: Pins].self, from: data!)
            
            myStructDictionary.forEach { print("\($0.key): \($0.value)") } // decoded!!!!!
        } catch let error as NSError {
            print(error)
        }

    }
    
    func getAllProducts(){
        
        
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            let URL = "http://www.majalahjualbeli.com/api/pin/get-wedding"
            Alamofire.request(URL)
                .responseObject { (response: DataResponse<ResultMap>) in
                    if let result = response.result.value {
                        // That was all... You now have a WeatherResponse object with data
                        self.resultSet = result
                        //print("HASILNYA: ", result)
                        
                        while self.countable < self.resultSet.pins.count{
                            //var dict = self.resultSet.pins[countable]
                            print("IDNYA ADALAH: " , self.resultSet.pins[self.countable].id!)
                            print("NOMINAL: ", self.resultSet.pins[self.countable].nominal!)
                            print("UPDATED: ", self.resultSet.pins[self.countable].updated_at!)
                            
                            let imageLabel = self.serverURL+(self.resultSet.pins[self.countable].produk?.url)!
                            
                            SDWebImageManager.shared().imageDownloader?.downloadImage(with: NSURL(string: imageLabel)! as URL, options: SDWebImageDownloaderOptions.allowInvalidSSLCertificates, progress: { (min, max, url) in
                                //print("loading…… \(imageLabel)")
                            }, completed: { (image, data, error, finished) in
                                if image != nil {
                                    //self.itemHeight[self.countable] = Int((image?.size.height)!)
                                    //print("finished....\(image?.size.height ?? 0)")
                                    self.itemHeight.append(Int((image?.size.height)!))
                                    //print("IMAGE Countable: \(self.itemHeight[self.imageCountable])")
                                    
                                    if self.imageCountable == self.resultSet.pins.count-1{
                                        print("SELESAI HITUNGNYA")
                                        //self.collectionView?.reloadData();
                                        self.collectionView?.backgroundColor = UIColor.clear
                                        self.collectionView?.contentInset = UIEdgeInsets(top: 23, left: 10, bottom: 10, right: 10)
                                        // Set the PinterestLayout delegate
                                        if let layout = self.collectionView?.collectionViewLayout as? PinterestLayout {
                                            layout.delegate = self
                                            self.collectionView?.reloadData()
                                            self.collectionView?.setNeedsDisplay()
                                        }
                                    }
                                    
                                    self.imageCountable = self.imageCountable + 1
                                    //self.collectionView?.reloadData()
                                } else {
                                    print("wrong")
                                }
                            })
                        
                            
                            self.countable = self.countable + 1
                        }
                        print("Jumlahnya adalah: \(self.countable)")
                    }
            }
            
            DispatchQueue.main.async {
                print("This is run on the main queue, after the previous code in outer block")
                //print("Jumlahnya adalah: \(self.countable)")
                //self.collectionView?.reloadData();
            }
        }
    }

}

extension HomeViewController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.countable
        //return photos.count
        //return self.resultSet.pins.count
        //return self.itemHeight.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnotatedPhotoCell", for: indexPath)
        //if let annotateCell = cell as? AnnotatedPhotoCell {
            //annotateCell.photo = photos[indexPath.item]
            //annotateCell.photo?.caption = self.resultSet.pins[indexPath.row].produk?.judul
            //annotateCell.captionLabel.text = self.resultSet.pins[indexPath.row].produk?.judul
        //}
        
        if let annotateCell = cell as? AnnotatedPhotoCell {
            //annotateCell.photo = photos[indexPath.item]
            //annotateCell.photo?.caption = self.resultSet.pins[indexPath.row].produk?.judul
            //annotateCell.captionLabel.text = self.resultSet.pins[indexPath.row].produk?.judul
            let captionLabel = self.resultSet.pins[indexPath.row].produk?.judul
            let imageLabel = self.serverURL+(self.resultSet.pins[indexPath.row].produk?.url)!
            annotateCell.captionLabel.text = captionLabel
            annotateCell.commentLabel.text = imageLabel
            //let imageURL = URL(string: imageLabel)
            //annotateCell.imageView.image
            annotateCell.imageView.sd_setImage(with: URL(string: imageLabel), placeholderImage: UIImage(named: "Asuransi"))
            annotateCell.delegate = self as? MyCellDelegate
        }
        return cell
    }
    
}

//MARK: - PINTEREST LAYOUT DELEGATE
extension HomeViewController : PinterestLayoutDelegate {
    
    // 1. Returns the photo height
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        //return photos[indexPath.item].image.size.height
        return CGFloat(self.itemHeight[indexPath.row]/2)
        //return 200;
    }
    
}
