//
//  ProfileViewController.swift
//  Majalah JualBeli
//
//  Created by Yacob Madiana on 11/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import SwiftyJSON

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var goToProdukButton: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var serverURL = "http://www.majalahjualbeli.com/"
        var profilUrl = "http://majalahjualbeli.com/api/profil/get-data/"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let URL = "http://majalahjualbeli.com/api/profil/get-data/useroto0033"
        
        Alamofire.request(URL, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON: \(json["profil"])")
                print("JUMLAH PROFIL: \(json["profil"].count)")
                // Int
                if let id = json["profil"]["nama_lengkap"].string {
                    // Do something you want
                    print("HEHEHE: \(id)")
                } else {
                    // Print the error
                    print(json["errors"].error!)
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Methods
    
    @IBAction func produkButtonPressed(_ sender: UIButton) {
        
    }
    

}
