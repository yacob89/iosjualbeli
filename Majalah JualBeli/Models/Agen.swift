//
//  Agen.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/9/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

    struct Agen {
        
        //MARK: Properties
        
        var id: Int
        var profil: Int
        var createdAt: Date
        var updatedAt: Date
        var createdBy: Int
        var updatedBy: Int
        
        //MARK: Initialization
        
        init(id:Int, profil: Int, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int) {
            
            // Initialize stored properties.
            self.id = id
            self.profil = profil
            self.createdAt = createdAt
            self.updatedAt = updatedAt
            self.createdBy = createdBy
            self.updatedBy = updatedBy
        }
    
    }
