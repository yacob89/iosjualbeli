//
//  Bagian.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/9/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Bagian {
    
    //MARK: Properties
    
    var id: Int
    var nama: String
    var aktif: Bool
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    var karyawan: Karyawan
    
    //MARK: Initialization
    
    init(id:Int, nama: String, aktif:Bool, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int, karyawan:Karyawan) {
        
        // Initialize stored properties.
        self.id = id
        self.nama = nama
        self.aktif = aktif
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
        self.karyawan = karyawan
    }
    
}
