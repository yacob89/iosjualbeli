//
//  Beriklan.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/9/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Beriklan {
    
    //MARK: Properties
    
    var id: Int
    var nama: String
    var perusahaan: String
    var email: String
    var telepon: String
    var subscribe: Int
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    
    //MARK: Initialization
    
    init(id:Int, nama: String, perusahaan: String, email: String, telepon: String, subscribe:Int, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int) {
        
        // Initialize stored properties.
        self.id = id
        self.nama = nama
        self.perusahaan = perusahaan
        self.email = email
        self.telepon = telepon
        self.subscribe = subscribe
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
    }
}
