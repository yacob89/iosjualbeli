//
//  Edisi.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/9/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Edisi {
    
    //MARK: Properties
    
    var id: Int
    var nomor: String
    var aktif: Bool
    var logo: String
    var kategori: String
    var caption: String
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    
    //MARK: Initialization
    
    init(id:Int, nomor: String, aktif: Bool, logo: String, kategori: String, caption:String, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int) {
        
        // Initialize stored properties.
        self.id = id
        self.nomor = nomor
        self.aktif = aktif
        self.logo = logo
        self.kategori = kategori
        self.caption = caption
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
    }
}
