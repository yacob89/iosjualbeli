//
//  Halaman.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/9/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Halaman {
    
    //MARK: Properties
    
    var id: Int
    var edisi_id : Int
    var nomor: String
    var nama_file: String
    var aktif: Int
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    
    //MARK: Initialization
    
    init(id:Int, edisi_id: Int, nomor: String, nama_file: String, aktif:Int, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int) {
        
        // Initialize stored properties.
        self.id = id
        self.edisi_id = edisi_id
        self.nomor = nomor
        self.nama_file = nama_file
        self.aktif = aktif
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
    }
}
