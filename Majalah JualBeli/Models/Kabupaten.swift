//
//  Kabupaten.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/9/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Kabupaten {
    
    //MARK: Properties
    
    var id: String
    var nama: String
    
    //MARK: Initialization
    
    init(id:String, nama: String) {
        
        // Initialize stored properties.
        self.id = id
        self.nama = nama
    }
}
