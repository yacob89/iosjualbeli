//
//  KabupatenMap.swift
//  Majalah JualBeli
//
//  Created by Yacob Madiana on 11/7/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import Foundation
import EVReflection

class KabupatenMap: EVNetworkingObject {
    var id: String?
    var nama: String?
}
