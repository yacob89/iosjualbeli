//
//  Karyawan.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/9/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Karyawan {
    
    //MARK: Properties
    
    var id: Int
    var foto: String
    var tanggalJoin: Date
    var nama: String
    var alamat: String
    var telepon: String
    var fullTime: Bool
    var gajiPerJam: Float
    var aktif: Bool
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    var bagian: Int
    var gaji: Int
    var file: UIImage
    
    //MARK: Initialization
    
    init(id:Int, foto:String, tanggalJoin: Date, nama:String, alamat: String, telepon: String, fullTime: Bool, gajiPerJam:Float, aktif: Bool, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int, bagian:Int, gaji:Int, file:UIImage) {
        
        // Initialize stored properties.
        self.id = id
        self.foto = foto
        self.tanggalJoin = tanggalJoin
        self.nama = nama
        self.alamat = alamat
        self.telepon = telepon
        self.fullTime = fullTime
        self.gajiPerJam = gajiPerJam
        self.aktif = aktif
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
        self.bagian = bagian
        self.gaji = gaji
        self.file = file
    }
}
