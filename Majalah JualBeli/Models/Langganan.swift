//
//  Langganan.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/9/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Langganan {
    
    //MARK: Properties
    
    var id: Int
    var nama: String
    var email: String
    var alamat: String
    var telepon: String
    var kodePos: String
    var kota: String
    var tanggalLahir: Date
    var tipeLangganan: String
    var orangAgen: Int
    var orangTeman: Int
    var orangChat: Int
    var orangLain: String
    var sosmedFacebook: Int
    var sosmedInstagram: Int
    var sosmedTwitter: Int
    var sosmedPinterest: Int
    var sosmedPath: Int
    var sosmedLain: String
    var cetakRumah: Int
    var cetakBank: Int
    var cetakSekolah: Int
    var cetakSupermarket: Int
    var cetakLain: String
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    
    //MARK: Initialization
    
    init(id:Int, nama: String, email: String, alamat:String, telepon:String, kodePos: String, kota: String, tanggalLahir: Date, tipeLangganan: String, orangAgen: Int, orangTeman: Int, orangChat: Int, orangLain: String, sosmedFacebook: Int, sosmedInstagram: Int, sosmedTwitter: Int, sosmedPinterest: Int, sosmedPath: Int, sosmedLain: String, cetakRumah: Int, cetakBank: Int, cetakSekolah: Int, cetakSupermarket: Int, cetakLain: String, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int) {
        
        // Initialize stored properties.
        self.id = id
        self.nama = nama
        self.email = email
        self.alamat = alamat
        self.telepon = telepon
        self.kodePos = kodePos
        self.kota = kota
        self.tanggalLahir = tanggalLahir
        self.tipeLangganan = tipeLangganan
        self.orangAgen = orangAgen
        self.orangTeman = orangTeman
        self.orangChat = orangChat
        self.orangLain = orangLain
        self.sosmedFacebook = sosmedFacebook
        self.sosmedInstagram = sosmedInstagram
        self.sosmedTwitter = sosmedTwitter
        self.sosmedPinterest = sosmedPinterest
        self.sosmedPath = sosmedPath
        self.sosmedLain = sosmedLain
        self.cetakRumah = cetakRumah
        self.cetakBank = cetakBank
        self.cetakSekolah = cetakSekolah
        self.cetakSupermarket = cetakSupermarket
        self.cetakLain = cetakLain
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
    }
}
