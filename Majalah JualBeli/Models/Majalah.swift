//
//  Majalah.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/9/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Majalah {
    
    //MARK: Properties
    
    var id: Int
    var kotaId: Int
    var nama: String
    var volume: String
    var keterangan: String
    var aktif: Int
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    
    //MARK: Initialization
    
    init(id:Int, kotaId: Int, nama: String, volume: String, keterangan: String, aktif: Int, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int) {
        
        // Initialize stored properties.
        self.id = id
        self.kotaId = kotaId
        self.nama = nama
        self.volume = volume
        self.keterangan = keterangan
        self.aktif = aktif
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
    }
}
