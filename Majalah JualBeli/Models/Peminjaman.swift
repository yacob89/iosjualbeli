//
//  Peminjaman.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Peminjaman {
    
    //MARK: Properties
    
    var id: Int
    var vendorId: Int
    var tanggal: Date
    var nominal: Float
    var lunas: Int
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    
    //MARK: Initialization
    
    init(id:Int, vendorId: Int, tanggal: Date, nominal: Float, lunas: Int, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int) {
        
        // Initialize stored properties.
        self.id = id
        self.vendorId = vendorId
        self.tanggal = tanggal
        self.nominal = nominal
        self.lunas = lunas
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
    }
}
