//
//  Phone.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Phone {
    
    //MARK: Properties
    
    var mobile: String
    var home: String
    var office: String
    
    //MARK: Initialization
    
    init(mobile: String, home: String, office: String) {
        
        // Initialize stored properties.
        self.mobile = mobile
        self.home = home
        self.office = office
    }
}
