//
//  Pin.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Pin {
    
    //MARK: Properties
    
    var id: String
    var nominal: String
    var updatedAt: String
    var like: String
    var likes: String
    var produk: Produk
    var profil: Profil
    
    //MARK: Initialization
    
    init(id: String, nominal: String, updatedAt: String, like: String, likes: String, produk: Produk, profil: Profil) {
        
        // Initialize stored properties.
        self.id = id
        self.nominal = nominal
        self.updatedAt = updatedAt
        self.like = like
        self.likes = likes
        self.produk = produk
        self.profil = profil
    }

}
