//
//  PinMap.swift
//  Majalah JualBeli
//
//  Created by Yacob Madiana on 11/7/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import Foundation
import EVReflection

class PinMap: EVNetworkingObject {
    var id: String?
    var nominal: String?
    var updated_at: String?
    var produk: ProdukMap?
    var profil: ProfilMap?
    var like: String?
    var likes: String?
}
