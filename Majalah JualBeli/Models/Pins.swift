//
//  Produk.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Pins: Codable {
    
    //MARK: Properties
    
    var berkas: String
    var url: String
    var id: String
    var judul: String
    var deskripsi: String
    var tipe: String
    var edisi: String
    
    init?(json: [String: Any]) {
        guard let berkas = json["berkas"] as? String,
            let url = json["url"] as? String,
            let id = json["id"] as? String,
            let judul = json["judul"] as? String,
            let deskripsi = json["deskripsi"] as? String,
            let tipe = json["tipe"] as? String,
            let edisi = json["edisi"] as? String else {
                return nil
        }
        self.berkas = berkas
        self.url = url
        self.id = id
        self.judul = judul
        self.deskripsi = deskripsi
        self.tipe = tipe
        self.edisi = edisi
    }
    
    static func endpointForID(_ id: Int) -> String {
        return "https://jsonplaceholder.typicode.com/todos/\(id)"
    }
    
    // Get Array of Objects
    
    static func endpointForTodos() -> String {
        //return "https://jsonplaceholder.typicode.com/todos/"
        return "http://www.majalahjualbeli.com/api/pin/get-wedding"
    }
    
    enum BackendError: Error {
        case urlError(reason: String)
        case objectSerialization(reason: String)
    }

}

