//
//  Presensi.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Presensi {
    
    //MARK: Properties
    
    var id: Int
    var karyawanId: Int
    var masuk: Date
    var waktu: Date
    var status: Int
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    
    //MARK: Initialization
    
    init(id:Int, karyawanId: Int, masuk: Date, waktu: Date, status: Int, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int) {
        
        // Initialize stored properties.
        self.id = id
        self.karyawanId = karyawanId
        self.masuk = masuk
        self.waktu = waktu
        self.status = status
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
    }
}
