//
//  Produk.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Produk: Codable {
    
    //MARK: Properties
    
    var berkas: String
    var url: String
    var id: String
    var judul: String
    var deskripsi: String
    var tipe: String
    var edisi: String
    
    /*var title: String
    var id: Int?
    var userId: Int
    var completed: Int*/
    
    //MARK: Initialization
    
    /*init(berkas: String, url: String, id: String, judul: String, deskripsi: String, tipe: String, edisi: String) {
        
        // Initialize stored properties.
        self.berkas = berkas
        self.url = url
        self.id = id
        self.judul = judul
        self.deskripsi = deskripsi
        self.tipe = tipe
        self.edisi = edisi
    }*/
    
    init?(json: [String: Any]) {
        guard let berkas = json["berkas"] as? String,
            let url = json["url"] as? String,
            let id = json["id"] as? String,
            let judul = json["judul"] as? String,
            let deskripsi = json["deskripsi"] as? String,
            let tipe = json["tipe"] as? String,
            let edisi = json["edisi"] as? String else {
                return nil
        }
        self.berkas = berkas
        self.url = url
        self.id = id
        self.judul = judul
        self.deskripsi = deskripsi
        self.tipe = tipe
        self.edisi = edisi
    }
    
    /*init?(json: [String: Any]) {
        guard let title = json["title"] as? String,
            let id = json["id"] as? Int,
            let userId = json["userId"] as? Int,
            let completed = json["completed"] as? Int else {
                return nil
        }
        self.title = title
        self.userId = userId
        self.completed = completed
        self.id = id
    }*/
    
    // Get Single JSON Object
    
    static func endpointForID(_ id: Int) -> String {
        return "https://jsonplaceholder.typicode.com/todos/\(id)"
        //return "http://www.majalahjualbeli.com/api/pin/get-wedding\(id)"
    }
    
    // Get Array of Objects
    
    static func endpointForTodos() -> String {
        //return "https://jsonplaceholder.typicode.com/todos/"
        return "http://www.majalahjualbeli.com/api/pin/get-wedding"
    }
    
    enum BackendError: Error {
        case urlError(reason: String)
        case objectSerialization(reason: String)
    }
    
    static func produkByID(_ id: Int, completionHandler: @escaping (Produk?, Error?) -> Void) {
        // set up URLRequest with URL
        let endpoint = Produk.endpointForID(id)
        guard let url = URL(string: endpoint) else {
            print("Error: cannot create URL")
            let error = BackendError.urlError(reason: "Could not construct URL")
            completionHandler(nil, error)
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // Make request
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler: {
            (data, response, error) in
            // handle response to request
            // check for error
            guard error == nil else {
                completionHandler(nil, error!)
                return
            }
            // make sure we got data in the response
            guard let responseData = data else {
                print("Error: did not receive data")
                let error = BackendError.objectSerialization(reason: "No data in response")
                completionHandler(nil, error)
                return
            }
            
            // parse the result as JSON
            // then create a Todo from the JSON
            do {
                if let produkJSON = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any],
                    let todo = Produk(json: produkJSON) {
                    // created a TODO object
                    completionHandler(todo, nil)
                } else {
                    // couldn't create a todo object from the JSON
                    let error = BackendError.objectSerialization(reason: "Couldn't create a todo object from the JSON")
                    completionHandler(nil, error)
                }
            } catch {
                // error trying to convert the data to JSON using JSONSerialization.jsonObject
                completionHandler(nil, error)
                return
            }
        })
        task.resume()
    }
    
    static func allProduks(completionHandler: @escaping ([Produk]?, Error?) -> Void) {
        let endpoint = Produk.endpointForTodos()
        guard let url = URL(string: endpoint) else {
            print("Error: cannot create URL")
            let error = BackendError.urlError(reason: "Could not construct URL")
            completionHandler(nil, error)
            return
        }
        let urlRequest = URLRequest(url: url)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            guard let responseData = data else {
                print("Error: did not receive data")
                completionHandler(nil, error)
                return
            }
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            let decoder = JSONDecoder()
            do {
                print(response)
                let produks = try decoder.decode([Produk].self, from: responseData)
                //produks.forEach { print("\($0.judul): \($0.berkas)") }
                //let produks = try JSONDecoder().decode([Produk].self, from: responseData)
                completionHandler(produks, nil)
            } catch {
                print("error trying to convert data to JSON")
                print(error)
                completionHandler(nil, error)
            }
        }
        task.resume()
    }
}
