//
//  ProdukMap.swift
//  Majalah JualBeli
//
//  Created by Yacob Madiana on 11/7/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import Foundation
import EVReflection

class ProdukMap: EVNetworkingObject {
    var url: String?
    var id: String?
    var judul: String?
    var deskripsi: NSString?
    var tipe: String?
}
