//
//  Profil.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Profil {
    
    //MARK: Properties
    
    var id: String
    var userId: String
    var foto: String
    var jenisKelamin: String
    var telepon1: String
    var telepon2: String
    var namaPerusahaan: String
    var website: String
    var namaLengkap: String
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    var user: User
    var kabupaten: Kabupaten
    
    //MARK: Initialization
    
    init(id:String, userId: String, foto: String, jenisKelamin: String, telepon1: String, telepon2: String, namaPerusahaan: String, website: String, namaLengkap: String, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int, user: User, kabupaten: Kabupaten) {
        
        // Initialize stored properties.
        self.id = id
        self.userId = userId
        self.foto = foto
        self.jenisKelamin = jenisKelamin
        self.telepon1 = telepon1
        self.telepon2 = telepon2
        self.namaPerusahaan = namaPerusahaan
        self.website = website
        self.namaLengkap = namaLengkap
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
        self.user = user
        self.kabupaten = kabupaten
    }
}
