//
//  ProfilMap.swift
//  Majalah JualBeli
//
//  Created by Yacob Madiana on 11/7/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import Foundation
import EVReflection

class ProfilMap: EVNetworkingObject {
    var url: String?
    var id: String?
    var nama_lengkap: String?
    var telepon1: String?
    var user: UserMap?
    var kabupaten: KabupatenMap?
}
