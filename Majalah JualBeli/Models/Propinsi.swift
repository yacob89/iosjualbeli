//
//  Propinsi.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Propinsi{
    
    //MARK: Properties
    
    var id: Int
    var nama: String
    var aktif: Int
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    
    //MARK: Initialization
    
    init(id:Int, nama: String, aktif: Int, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int) {
        
        // Initialize stored properties.
        self.id = id
        self.nama = nama
        self.aktif = aktif
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
    }
}
