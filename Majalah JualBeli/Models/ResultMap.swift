//
//  ResultMap.swift
//  Majalah JualBeli
//
//  Created by Yacob Madiana on 11/7/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import Foundation
import EVReflection

class ResultMap: EVNetworkingObject {
    var result: String?
    var pins: [PinMap] = [PinMap]()
}
