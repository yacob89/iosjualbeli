//
//  Role.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Role{
    
    //MARK: Properties
    
    var id: Int
    var name: String
    var symName: String
    var publish: Int
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    
    //MARK: Initialization
    
    init(id:Int, name: String, symName: String, publish: Int, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int) {
        
        // Initialize stored properties.
        self.id = id
        self.name = name
        self.symName = symName
        self.publish = publish
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
    }
}
