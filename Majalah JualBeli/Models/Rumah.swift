//
//  Rumah.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Rumah {
    
    //MARK: Properties
    
    var id: Int
    var pemilikId: Int
    var agenId: Int
    var alamat: String
    var luasTanah: Double
    var jumlahLantai: Int
    var kamarTidur: Int
    var kamarMandi: Int
    var listrik: Double
    var harga: Float
    var luasBangunan: Double
    var usiaBangunan: Double
    var kapasitasGarasi: Double
    var sumberAir: String
    var statusKepemilikan: Int
    var aktif: Int
    var primary: String
    var judul: String
    var fitur: String
    
    //MARK: Initialization
    
    init(id:Int, pemilikId: Int, agenId: Int, alamat: String, luasTanah: Double, jumlahLantai: Int, kamarTidur: Int, kamarMandi: Int, listrik: Double, harga: Float, luasBangunan: Double, usiaBangunan: Double, kapasitasGarasi: Double, sumberAir: String, statusKepemilikan: Int, aktif: Int, primary: String, judul: String, fitur: String) {
        
        // Initialize stored properties.
        self.id = id
        self.pemilikId = pemilikId
        self.agenId = agenId
        self.alamat = alamat
        self.luasTanah = luasTanah
        self.jumlahLantai = jumlahLantai
        self.kamarTidur = kamarTidur
        self.kamarMandi = kamarMandi
        self.listrik = listrik
        self.harga = harga
        self.luasBangunan = luasBangunan
        self.usiaBangunan = usiaBangunan
        self.kapasitasGarasi = kapasitasGarasi
        self.sumberAir = sumberAir
        self.statusKepemilikan = statusKepemilikan
        self.aktif = aktif
        self.primary = primary
        self.judul = judul
        self.fitur = fitur
    }
}
