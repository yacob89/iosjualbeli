//
//  Testing.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Testing {
    
    //MARK: Properties
    
    var id: String
    var name: String
    var email: String
    var address: String
    var gender: String
    var phone: Phone
    
    //MARK: Initialization
    
    init(id: String, name: String, email: String, address: String, gender: String, phone: Phone) {
        
        // Initialize stored properties.
        self.id = id
        self.name = name
        self.email = email
        self.address = address
        self.gender = gender
        self.phone = phone
    }
}
