//
//  Transaksi.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct Transaksi {
    
    //MARK: Properties
    
    var id: Int
    var keterangan: String
    var tanggal: Date
    var nominal: Float
    var tipe: String
    var pinjamanId: Int
    var gajiId: Int
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    
    //MARK: Initialization
    
    init(id:Int, keterangan: String, tanggal: Date, nominal: Float, tipe: String, pinjamanId: Int, gajiId: Int, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int) {
        
        // Initialize stored properties.
        self.id = id
        self.keterangan = keterangan
        self.tanggal = tanggal
        self.nominal = nominal
        self.tipe = tipe
        self.pinjamanId = pinjamanId
        self.gajiId = gajiId
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
    }
}
