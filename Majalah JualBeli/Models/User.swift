//
//  User.swift
//  Majalah JualBeli
//
//  Created by Elisa Hananya Sutanto on 10/10/17.
//  Copyright © 2017 CV Philos. All rights reserved.
//

import UIKit

struct User {
    
    //MARK: Properties
    
    var id: Int
    var roleId: Int
    var username: String
    var password: String
    var active: Int
    var email: String
    var createdAt: Date
    var updatedAt: Date
    var createdBy: Int
    var updatedBy: Int
    
    //MARK: Initialization
    
    init(id:Int, roleId: Int, username: String, password: String, active: Int, email: String, createdAt: Date, updatedAt: Date, createdBy: Int, updatedBy: Int) {
        
        // Initialize stored properties.
        self.id = id
        self.roleId = roleId
        self.username = username
        self.password = password
        self.active = active
        self.email = email
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.createdBy = createdBy
        self.updatedBy = updatedBy
    }
}
